package com.spring.hdiv.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.spring.hdiv.model.signupForm;
import com.spring.hdiv.service.SignUpService;

@Controller
public class SignController {

	@Autowired
    SignUpService sampleService;


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView index() {
        return new ModelAndView("index", "signupForm", new signupForm());
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String search(Model model, @Valid signupForm signupForm, BindingResult result, RedirectAttributes redirectAttributes) {


        if (result.hasErrors()) {
            return "index";
        }

        model.addAttribute("form", sampleService.saveForm(signupForm));
        return "show";
    }


    @RequestMapping(value = "/security-error", method = RequestMethod.GET)
    public String securityError(RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("page_error", "You do have have permission to do that!");
        return "redirect:/";
    }
}
